/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * 37 left
 * 38 up
 * 39 right
 * 40 down
 */

// Test

var length = 36;
var height = 22;
var matrix;
var pos_x = 18;
var pos_y = 11;
var del_x = pos_x;
var del_y = pos_y;
var clock;
var direction = 39;
var nb_moves = 0;
var snake_length = 4;
var inplay = false;

function init()
{
    var html = "";
    for(var i = 0 ; i < length * height  ; i++)
    {
        var column = i % length;
        var row = (i-column)/length;
        var html = html + "\n" + "<div class='white block row"+row+" column"+column+"'>*</div>";
    }   
    $(".cont").append(html);
    matrix = initMatrix();
    //show(0,35);
    displayMatrix();
    startGame();
    spawnMyTreats();
}

function getBlock(row, column)
{
    displayMatrix();    
    return $(".row"+row+".column"+column);
}

function show(row, column)
{
    getBlock(row, column).css("color", "black");
}

function hide(row, column)
{
    matrix[column][row] = 0;
    getBlock(row, column).css("color", "white");
}

function change(row, column)
{
    if(getBlock(row,column).css("color") === "rgb(255, 255, 255)")
        hide(row, column);
    else
        show(row, column);

}

function initMatrix()
{
    var matrix = [];
    for(var i = 0; i < length; i++)
        matrix[i] = new Array(height);
    for(var i = 0; i < length ; i++)
        for(var j = 0 ; j < height ; j++)
            matrix[i][j] = 0;
    return matrix;
}

function displayMatrix()
{
    $(".matrix").html("");
    var line;
    for(var i = 0 ; i < height; i++)
    {
        line = "";
        for(var j = 0; j < length ; j++)
            line += matrix[j][i];
        line = line +  "<br>";
        $(".matrix").append(line);
    }
}

function startGame()
{
    show(pos_y, pos_x);
    listen();
    pausePlay();
}

function listen()
{
    $("body").unbind("keydown").keydown(function(e){
        $(this).unbind("keydown");
        var code = e.keyCode || e.which;
        stop(code);
    });
}

function play()
{
    inplay = true;
    nb_moves++;
    matrix[pos_x][pos_y] = direction - 36;
    switch(direction)
    {
        case 37:
            pos_x--;
            break;
        case 38:
            pos_y--;    
            break;
        case 39:
            pos_x++;
            break;
        case 40:
            pos_y++;
            break;
        default:
            break;
    }
    if(pos_x < 0 || pos_x >= length || pos_y < 0 || pos_y >= height || matrix[pos_x][pos_y] > 0)
        stopTheGameBitch();
    else if (matrix[pos_x][pos_y] === -1)
        spawnMyTreats();
    show(pos_y,pos_x);
    var dx = 0, dy = 0;
    if(nb_moves > snake_length)
    {
        switch(matrix[del_x][del_y])
        {
            case 1:
                dx = -1;
                break;
            case 2:
                dy = -1;
                break;
            case 3:
                dx = 1;
                break;
            case 4:
                dy = 1;
                break;
            default:
                break;
        }
        hide(del_y, del_x);
    }
    del_x += dx;
    del_y += dy;
    inplay = false;
    listen();
    $(".matrix").append(del_x+" ").append(del_y+" ").append(pos_x+" ").append(pos_y);
    
}

function stopTheGameBitch()
{
    error("BOUMMM");
    clearInterval(clock);
    clock = null;
    $("body").unbind("keydown");
}

function stop(code)
{
    if(!inplay)
    {
        if(code <= 40 && code >= 37 && (direction + code) % 2 !== 0)
            direction = code;
        if(code === 32)
            pausePlay();        
    }
}

function pausePlay()
{
    if(!clock)
        clock = setInterval(play,100);
    else
    {
        clearInterval(clock);
        clock = null;
    }
}

function spawnMyTreats()
{
    var t_x, t_y;
    do
    {
        t_x = Math.floor((Math.random() * length));
        t_y = Math.floor((Math.random() * height));
    }
    while (matrix[t_x][t_y] !== 0)
    print(t_x);
    print(t_y);
    show(t_y, t_x);
    matrix[t_x][t_y] = -1;
}

function print(msg)
{
    console.log(msg);
}

function error(msg)
{
    $(".errors").html(msg);
}


